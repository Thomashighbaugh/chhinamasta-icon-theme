#!/bin/env bash
##########################################################
## TITLE:
##
## DESCRIPTION:
##
##
## AUTHOR: Thomas Leon Highbaugh <admin@thomasleonhighbaugh.me>
##########################################################

sed -i 's/ffffff/f4f4f7/g' **/*.svg

sed -i 's/222c31/22262d/g' **/*.svg

sed -i 's/e4e4e4/b2bfd9/g' **/*.svg

sed -i 's/e4e4e4/b2bfd9/g' **/*.svg

sed -i 's/607d8b/555e70/g' **/*.svg

sed -i 's/c2c2c2/b2bfs9/g' **/*.svg

sed -i 's/3f3f3f/17191e/g' **/*.svg

sed -i 's/5294e2/00caff/g' **/*.svg

sed -i 's/4877b1/0badff/g' **/*.svg

sed -i 's/5294e2/00caff/g' **/*.svg

sed -i 's/value_dark/3c3f4c/g' **/*.svg

sed -i 's/value_light/555e70/g' **/*.svg
